function datePicker() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {"firstDay": 1, "minDate": new Date()});
}

function getDatePicker() {
    return document.getElementsByClassName("datepicker")[0].value;
}