import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  constructor() { }

  todos = Todos;

  ngOnInit(): void {

  }

  removeTodo(todoName: string) {
    var Todos = JSON.parse(localStorage.getItem("todos"));
    let i = 0; let found = false;
    while (i < Todos.length && found == false) {
      if (Todos[i].name == todoName) {
        console.log("removed" + Todos[i].name);
        Todos.splice(i, 1);
        found = true;
      }
      i++;
    }
    localStorage.setItem("todos", JSON.stringify(Todos));
    window.location.reload();
  }
}

// const Todos = [
//   {name:'Superman', date: new Date()},
//   {name:'Batman', date: new Date()}
// ];
// localStorage.setItem("todos", JSON.stringify(Todos));

var Todos = JSON.parse(localStorage.getItem("todos"));