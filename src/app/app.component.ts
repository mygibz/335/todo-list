import { Component } from '@angular/core'

declare function datePicker(): any;
declare function getDatePicker(): string;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todo-list';

  public addingName: string;
  public addingDate: string;

  ngOnInit() {
    datePicker();
  }

  addTodo() {
    console.log(this.addingDate);
    var Todos = JSON.parse(localStorage.getItem("todos"));
    Todos.push({name:this.addingName, date: getDatePicker()})
    localStorage.setItem("todos", JSON.stringify(Todos));
    window.location.reload();
  }
}
